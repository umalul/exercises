<?php
class cHtml{
    private $title = "[Title goes here]";
    protected $body = "[Body goes here]";
    public function view(){
        echo "<html>
            <head>
                <title>$this->title</title>
            </head>
            <body>
                <p style = 'color:$this->color;font-size:$this->fontSize';>$this->body</p>
            </body>
            </html>";
    }
    function __construct($title = "",$body = ""){
        if($title != ""){
            $this->title = $title;
        }
            if($body != ""){
                $this->body = $body;
            }
    }
}
class textColored extends cHtml{
    protected $color;
    public function __set($property,$value){
        if($property == 'color'){
            $color = array('black','yellow','green','blue','white');
            if(in_array($value , $color)){
                $this->color = $value;
            }
                else{
                     die ("The color you choose is not available, Please choose another color");
                }
            }
    } 
}
class textSize extends textColored{
    protected $fontSize;
    public function __set($property,$value){
        parent::__set($property,$value);
        if($property == 'fontSize'){
            $fontSize = range(10,24);
            if(in_array($value , $fontSize)){
                $this->fontSize = $value;
            }
                else{
                    die ("The size you choose is not available, Please choose number between 10 to 24");
                }
        }
        
    }
}
?>